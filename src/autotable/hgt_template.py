def get_header1():

    return r"""\documentclass[10pt,twocolumn,twoside]{scrartcl}
%\documentclass[twocolumn,twoside]{article}
%==============================================================================%

\usepackage[a4paper,top=2.5cm,bottom=2.5cm,left=1.5cm,right=1.5cm]{geometry}
\usepackage{graphicx}
\usepackage{array}%Benutzung der tabular-Umgebung
\usepackage{multirow}
\usepackage{tabu}
\usepackage{xcolor,colortbl}
\usepackage{booktabs}
\setlength{\columnsep}{2em}
\usepackage{makecell}
\usepackage{xhfill}

\usepackage[%
font={scriptsize,sf},
labelfont={scriptsize,bf},
format=hang,    
format=plain,
margin=0pt,
width=\textwidth,
]{caption}

\begin{document}

\begin{table*}[ht]
\small
\centering
\captionsetup{width=\textwidth}
\caption{Results for """

def get_caption():
    return '{sample}.'

def get_header2():
    return r"""}
\vspace{.5ex} 
\resizebox{\textwidth}{!}{%
\begin{tabu}{*{2}{c}@{\hskip 1cm}*{3}{c}@{\hskip 0.9cm}*{3}{c}@{\hskip 1cm}*{3}{c}@{\hskip 1cm}*{4}{c}}
\rowfont{\bfseries}
\multicolumn{2}{c}{\hspace*{-0.7cm} \xrfill[3pt]{0.5pt} ~Organism~\xrfill[3pt]{0.5pt}} & \multicolumn{3}{c}{\hspace*{-1cm} \xrfill[3pt]{0.5pt} ~Acceptor~\xrfill[3pt]{0.5pt}} &   \multicolumn{3}{c}{\hspace*{-1cm} \xrfill[3pt]{0.5pt} ~Donor~\xrfill[3pt]{0.5pt}} &   \multicolumn{3}{c}{\hspace*{-1cm} \xrfill[3pt]{0.5pt} ~Read Evidence~\xrfill[3pt]{0.5pt}}  &   \multicolumn{4}{c}{\hspace*{-1.2cm} \xrfill[3pt]{0.5pt} ~Evidence Filter~\xrfill[3pt]{0.5pt}\hspace*{-1cm}}  \\
Acceptor & Donor & Start      &  End      & Coverage &  Start      & End      & Coverage &   Split & Spanning &  Within    &  A-Cov & D-Cov & Spanning & Within \\
\midrule
"""

def get_tail():
    return r"""}
\end{table*}

\end{document}"""

def get_body1():
    return r"""\begin{table*}[ht]
\small
\centering
\captionsetup{width=\textwidth}
\caption{Results for """

def get_body2():
    return r"""\end{tabu}
}
\label{tab:"""

def get_label():
    return '{label}'

def get_body3():
    return r"""}
\end{table*}"""
