def get_length(fasta):
    with open(fasta, 'r') as f:
        length = 0
        seqnum = 0
        for line in f:
            if line[0] == '>':
                seqnum += 1
                if seqnum > 1:
                    raise(Exception('Reference file has multiple sequences, please remove plasmids before simulating!'))
                else:
                    pass
            else:
                length += len(line) - 1
    return int(length)
