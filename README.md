# DaisySuite

DaisySuite is a pipeline for horizontal gene transfer (HGT) detection using sequencing data.

[Documentation](http://daisysuite.readthedocs.io/en/latest/)

## Setup

#### With Conda
If you need to install Conda first, please refer to the [Conda quick install guide](https://conda.io/docs/install/quick.html).
Otherwise, run
```
conda install daisysuite
```
to install Daisysuite.

Alternatively, run
```
conda create -n <env_name> daisysuite
```
to install DaisySuite in a separate environment named <env_name> and use ```source activate <env_name>``` and ```source deactive``` to activate or deactive the environment, respectively.

##### Without Conda
Please refer to the [documentation](http://daisysuite.readthedocs.io/en/latest/).

##### Additional dependency for Laser
Laser needs ```xa2multi.pl``` to be present in PATH.<br>
It is part of bwa, but not installed by Conda.<br>
```xa2multi.pl``` is available [here](https://github.com/lh3/bwa/blob/master/xa2multi.pl).<br>
Add it to any directory present in your PATH, e.g.<br>
```
wget https://github.com/lh3/bwa/blob/master/xa2multi.pl
mv xa2multi.pl ~/bin
```

### Database Setup
Run the setup script. It will download the NCBI refseq, build the indices for MicrobeGPS and yara/bwa and create files used by MicrobeGPS.<br>
The NCBI database is located at ```DaisySuite/data/ncbi``` and the yara/bwa index at ```DaisySuite/data/bwa/bwa_index``` and ```DaisySuite/data/yara_index```, respectively.
```
DaisySuite_setup
```

### Usage
##### Preparation
If you want to simulate reads and then analyze them, you only need to specify the outputdir in the config.<br>
If you already have (gzipped) fastq reads, these files need to be present within the ```reads``` folder in the outputdir.<br>
Furthermore, the reads need to follow a specific naming scheme:<br>
```{sample}.1.fq``` for single end reads<br>
```{sample}.1.fq``` and ```{sample}.2.fq``` for paired end reads<br>
{sample} is an arbitrary name for the read data set<br>
The reads can be either in plain fastq format or gzipped (fq.gz).<br>
Please note that Laser cannot be used with single end reads.

The read names (the read names within the fastq file) should not contain a slash ('/') symbol.<br>
For example, simulated.1/1 will not work. (remove, e.g. by using ```sed -i 's/\(@simulated\.[0-9]*\)\/[0-9]*/\1/' <fq-file>```) <br>

###### Example directory

.<br>
├──&nbsp;outputdir<br>
│&nbsp;&nbsp;&nbsp;├──&nbsp;reads<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample1}.1.fq.gz<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample1}.2.fq.gz<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample2}.1.fq.gz<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample2}.2.fq.gz<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample3}.1.fq.gz<br>

All samples inside the outputdir will be run.

##### Config file
All necessary parameters are set in a configfile. An example config file is located at ```DaisySuite/config/example.yaml```

##### Running a job
This pipeline uses snakemake and therefore supports all parameters implemented by snakemake.

```
DaisySuite --configfile config/example.yaml
```

See ```DaisySuite -h``` for additional options.

### Output
The results of DaisyGPS are in the ```candidates``` directory.<br>
The results of Daisy are in the ```hgt_eval``` directory.


.<br>
├──&nbsp;outputdir<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;DaisySuite_log-{timestamp}.txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# logfile containing information about the run<br>
│&nbsp;&nbsp;&nbsp;├──&nbsp;reads<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample1}.1.fq.gz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# read file 1<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample1}.2.fq.gz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# read file 2<br>
│&nbsp;&nbsp;&nbsp;├──&nbsp;candidates<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample1}\_acceptors.fa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Sequences for all acceptor candidates<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample1}\_acceptors.txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Accession.Version for all acceptor candidates<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample1}\_candidates.tsv&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# All reported candidates including several metrics<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample1}\_complete.tsv&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# All mapped references including several metrics<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample1}\_donors.fa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Sequences for all donor candidates<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample1}\_donors.txt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Accession.Version for all donor candidates<br>
│&nbsp;&nbsp;&nbsp;├──&nbsp;hgt_eval<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample1}.fasta&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Sequences of filtered horizontally transferred regions<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample1}.tsv&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# All hgt regions<br>
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└──&nbsp;{sample1}.vcf&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# All filtered hgt regions<br>


Additonally, there are many intermediate directories, those can be kept with ```--nt```.

| Directory         | Description                                                          |
|-------------------|----------------------------------------------------------------------|
| bwa/yara          | [Daisy] contains index for acceptor/donor candidates                 |
| candidate_mapping | [Daisy] contains mapping of candidates against ncbi                  |
| fasta_npa         | [Daisy] contains not properly aligned reads in fasta format          |
| fastq_npa         | [Daisy] contains not properly aligned reads in fastq format          |
| gustaf            | [Daisy] contains results of gustaf                                   |
| hgt_candidates    | [Daisy] contains hgt candidates generated in the hgt evaluation step |
| hgt_eval_pair     | [Daisy] single result files, merged files are found in hgt_eval      |
| joined            | [Daisy] contains joined sequences of acceptor and donor candidates   |
| mapping           | [DaisyGPS] contains mapping of reads against ncbi                      |
| mgps              | [DaisyGPS] contains results of MicrobeGPS                              |
| npa_joined        | [Daisy] contains merged fasta_npa files                              |
| process           | [DaisyGPS] contains intermediate results of candidate detection        |
| sort_mapping      | [Daisy] contains sorted alignment file from candidate_mapping        |
| sort_npa          | [Daisy] contains sorted fasta file from npa_joined                   |
| stellar           | [Daisy] contains results of stellar                                  |
